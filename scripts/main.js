fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(films => {
    const filmsList = document.createElement('ul');
    films.forEach(film => {
      const filmItem = document.createElement('li');
      filmItem.textContent = `Episode ${film.episodeId}: ${film.name} - ${film.openingCrawl}`;
      filmsList.appendChild(filmItem);

      fetchAll(film.characters)
        .then(characters => {
          const charactersList = document.createElement('ul');
          characters.forEach(character => {
            const characterItem = document.createElement('li');
            characterItem.textContent = character.name;
            charactersList.appendChild(characterItem);
          });
          filmItem.appendChild(charactersList);
        });
    });
    document.body.appendChild(filmsList);
  });

async function fetchAll(urls) {
  const responses = await Promise.all(urls.map(url => fetch(url)));
  const data = await Promise.all(responses.map(response => response.json()));
  return data;
}

